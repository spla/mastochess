search_end: fi
search_move: mou
search_new: nova
search_games: jocs
search_send: envia
search_config: conf
search_help: ajuda
search_draw: taules
new_game_started: partida iniciada! Esperant jugador...
playing_with: jugues amb
your_turn: el teu torn
game_name: partida
chess_hashtag: #escacs 
send_error: error al enviar les anotacions :-(
game_number_anotations: les anotacions de la partida n.
anotations_sent: enviades amb èxit!
game_no_exists: la partida n.
cant_send_to_fediverse_account: per ara no és possible :-(
it_not_exists: no existeix...
game_already_started: ja tenies iniciada una partida!
wait_other_player: espera l'altre jugador
is_not_legal_move: és un moviment il·legal. Torna a tirar.
check_done: t'ha fet escac!
check_mate: Escac i mat! (en
check_mate_movements: moviments)
the_winner_is: El guanyador és:
well_done: ben jugat!
winned_games: Partides guanyades:
wins_of_many: de
lost_piece: * has perdut 
not_legal_move_str: moviment il·legal!
player_leave_game: ha deixat la partida amb
leave_waiting_game: has abandonat la partida en espera.
started_games: partides iniciades:
game_is_waiting: en espera...
game_is_on_going: (en joc)
no_on_going_games: cap partida en joc
is_not_your_turn: no és el teu torn.
is_the_turn_of: és el torn de
pawn_piece: un peó
knight_piece: un cavall
bishop_piece: l'alfil
rook_piece: una torre
queen_piece: la Dama
king_piece: el Rei
pawn_piece_letter: P
knight_piece_letter: C
bishop_piece_letter: A
rook_piece_letter: T
queen_piece_letter: D
king_piece_letter: R 
email_subject: Anotacions partida n.
start_or_join_a_new_game: nova (iniciar partida o unirse a una en espera)
move_a_piece: mou e2e3 (per exemple)
leave_a_game: fi (per a deixar la partida en qualsevol moment)
list_games: jocs (mostra un llistat de partides actives)
get_a_game_anotation: envia 1 (envia la partida en format pgn.)
show_help: ajuda (mostra aquesta ajuda i, per tant, és l'ajuda de l'ajuda)
stalemate_str: taules! partida finalitzada.
ask_for_draw: taules
claim_draw_str: ha proposat taules a
draw_and_str: i 
agreed_draw_str: han acordat taules.
claim_a_draw: taules (per a proposar/acceptar taules)
search_panel: panell
panel_title_str: Panell de
panel_games_str: Partides
panel_wins_str: Victòries
panel_ratio_str: Ràtio
post_my_panel_str: panell (publica les estadístiques)
locale_change_successfully: llengua canviada amb èxit a
locale_not_changed: encara no és suportada :-(
change_lang_str: conf ca (per a configurar el bot en català)
panel_elo_rating_str: Elo 
