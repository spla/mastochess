# Mastodon Chess
Play with other fediverse users a Chess game! Mastodon Chess control games, players and boards and even it post, graphically, every move to both players!  
Mastodon Chess (mastochess) uses [python-chess](https://python-chess.readthedocs.io/en/latest/) library.  
Mastodon Chess uses Elo rating system to calculate the relative skill levels of fediverse players!

### How to play:  

- To get help:  

@your_bot_username help

- To start a game:  

@your_bot_username new

- To make a move:

@your_bot_username move e2e4

- To finish game at any time:

@your_bot_username end  

- To list on going games:  

@your_bot_username games  

- To get any game anotations, in ex. game 1, in pgn format:  

@your_bot_username send 1

- To promote a pawn use first letter of desired piece:  

@your_bot_username move g7g8r (if you want a rook)  

n = knight  
b = bishop  
r = rook  

Don't use q for queen. Pawn is promoted to Queen by default.  

- To claim a draw:  

@your_bot_username draw  

- To get your panel stats:

@your_bot_username panel  

- To change the bot's language:  

@your_bot_username conf en

### Commands table

|   ca  |   en  |    es    |   fr     |  ex. |   Observ.   |
|:-----:|:-----:|:--------:|:--------:|:----:|:-----------:|
|  nova |  new  |   nueva  | nouvelle |      |             |
|  mou  |  move |   mueve  | déplace  | e2e3 |             |
|   fi  |  end  |    fin   |   fin    |      |             |
|  jocs | games | partidas | parties  |      |             |
| envia |  send |   envia  | envoyer  |  1   | game number |
| taules|  draw |  tablas  |  nulle   |      |             |
| ajuda |  help |   ayuda  |  aide    |      |             |
| panell| panel |   panel  | panneau  |      |             |
| conf  | conf  |   conf   |   conf   |      | ca,es,fr,en |

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account
-   [Mastodon](https://joinmastodon.org) server admin

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables in it.  

3. Run `python smtp_setup.py` to setup your smtp server, user and password. Needed to send game anotations to players.

4. Run `python setup.py` to get your Mastodon's bot account tokens.

5. Use your favourite scheduling method to set `python mastochess.py` to run regularly.

![board](board.png)  

20.11.2020 - New feature! Added link to on going games in games list  
21.11.2020 - New feature! Added a warning to player in turn when has been captured one of its pieces  
23.11.2020 - New feature! Now all moves are saved to file (with san anotation).  
25.11.2020 - New feature! Get any game anotations via email (see point 3 above).  
27.11.2020 - New feature! Pawn promotion and locales support ( ca & eng )  
28.11.2020 - New feature! Added 'es' locale support  
28.11.2020 - New feature! Now any fediverse user can play Mastodon Chess!  
28.11.2020 - New feature! Added help  
03.12.2020 - New feature! Added pgn save & send support  
04.12.2020 - New feature! Now players can claim a draw.  
05.12.2020 - New feature! Add panel stats.  
19.12.2020 - New feature! Now you can configure bot's language!  
19.12.2020 - New feature! Added french language!  
21.12.2020 - New feature! Added Elo rating system!
