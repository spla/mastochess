#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up chess parameters...")
    print("\n")
    chess_db = input("chess db name: ")
    chess_db_user = input("chess db user: ")
    mastodon_db = input("Mastodon database: ")
    mastodon_db_user = input("Mastodon database user: ")

    with open(file_path, "w") as text_file:
        print("chess_db: {}".format(chess_db), file=text_file)
        print("chess_db_user: {}".format(chess_db_user), file=text_file)
        print("mastodon_db: {}".format(mastodon_db), file=text_file)
        print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()

        print("Creating table.. "+table)
        # Create the table in PostgreSQL database
        cur.execute(sql)

        conn.commit()
        print("Table "+table+" created!")
        print("\n")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    chess_db = get_parameter("chess_db", config_filepath)
    chess_db_user = get_parameter("chess_db_user", config_filepath)

    ############################################################
    # create database
    ############################################################

    conn = None

    try:

        conn = psycopg2.connect(dbname='postgres',
            user=chess_db_user, host='',
            password='')

        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        cur = conn.cursor()

        print("Creating database " + chess_db + ". Please wait...")

        cur.execute(sql.SQL("CREATE DATABASE {}").format(
            sql.Identifier(chess_db))
        )
        print("Database " + chess_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ############################################################

    try:

        conn = None

        conn = psycopg2.connect(database = chess_db, user = chess_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

        # Load configuration from config file
        os.remove("config/db_config.txt")

        print("Exiting. Run db-setup again with right parameters")
        sys.exit(0)

    finally:

        if conn is not None:

            conn.close()

            print("\n")
            print("chess parameters saved to db-config.txt!")
            print("\n")

    ############################################################
    # Create needed tables
    ############################################################

    print("Creating table...")

    db = chess_db
    db_user = chess_db_user

    table = "botreplies"
    sql = "create table "+table+" (status_id bigint PRIMARY KEY, query_user varchar(40), status_created_at timestamptz)"
    create_table(db, db_user, table, sql)

    table = "games"
    sql = "create table "+table+" (created_at timestamptz, game_id serial, white_user varchar(40), black_user varchar(40), chess_game varchar(200), "
    sql +=" chess_status varchar(12), waiting boolean, updated_at timestamptz, next_move varchar(40), last_move varchar(40), moves int, finished boolean default False, "
    sql += "chess_link varchar(100), white_stalemate boolean default False, black_stalemate boolean default False, PRIMARY KEY(game_id))"
    create_table(db, db_user, table, sql)

    table = "stats"
    sql = "create table "+table+" (created_at timestamptz, game_id serial PRIMARY KEY, white_user varchar(40), black_user varchar(40), winner varchar(40), "
    sql += "finished boolean default False, updated_at timestamptz, CONSTRAINT fk_game FOREIGN KEY(game_id) REFERENCES games(game_id) ON DELETE CASCADE ON UPDATE CASCADE)"
    create_table(db, db_user, table, sql)

    table = "players"
    sql = "create table "+table+" (player_id bigint PRIMARY KEY, player_name varchar(40), lang varchar(2), elo_rating float)"
    create_table(db, db_user, table, sql)

    ############################################################

    print("Done!")
    print("Now you can run setup.py!")
    print("\n")
